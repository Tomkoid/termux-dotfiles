if status is-interactive
    # Commands to run in interactive sessions can go here
end

set fish_greeting # disable startup message
export GPG_TTY=$(tty) # show gpg password prompts in terminal
starship init fish | source
source ~/.bash_aliases
